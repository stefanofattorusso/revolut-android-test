package com.stefattorusso.revolutandroid.model

data class ErrorModel(
    val throwable: Throwable? = null,
    val code: Int? = 0,
    val resource: Any? = null
)