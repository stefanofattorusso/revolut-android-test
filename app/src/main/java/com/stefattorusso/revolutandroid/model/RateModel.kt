package com.stefattorusso.revolutandroid.model

data class RateModel(
    val code: String,
    val name: String,
    val value: String,
    val base: Boolean
)