package com.stefattorusso.revolutandroid.domain.usecase

import com.stefattorusso.revolutandroid.domain.RateDomain
import io.reactivex.Flowable

interface GetLatestUseCaseContract {

    fun getLatest(base: String): Flowable<List<RateDomain>>
}