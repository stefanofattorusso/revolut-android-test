package com.stefattorusso.revolutandroid.ui.main

import android.app.Activity

import com.stefattorusso.revolutandroid.base.BaseActivityModule
import com.stefattorusso.revolutandroid.di.scope.ActivityScope
import com.stefattorusso.revolutandroid.di.scope.FragmentScope
import com.stefattorusso.revolutandroid.ui.main.mvvm.MainFragment
import com.stefattorusso.revolutandroid.ui.main.mvvm.MainFragmentModule

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [BaseActivityModule::class])
abstract class MainActivityModule {

    @Binds
    @ActivityScope
    internal abstract fun activity(activity: MainActivity): Activity

    @FragmentScope
    @ContributesAndroidInjector(modules = [MainFragmentModule::class])
    internal abstract fun fragmentInjector(): MainFragment
}
