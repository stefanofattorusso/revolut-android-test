package com.stefattorusso.revolutandroid.ui.main

import android.os.Bundle
import com.stefattorusso.revolutandroid.R
import com.stefattorusso.revolutandroid.base.BaseActivity
import com.stefattorusso.revolutandroid.ui.main.mvvm.MainFragment
import com.stefattorusso.revolutandroid.utils.getFragment
import com.stefattorusso.revolutandroid.utils.loadFragment
import com.stefattorusso.revolutandroid.utils.newInstance

class MainActivity : BaseActivity() {

    private var mFragment: MainFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            mFragment = MainFragment().newInstance(intent.extras)
            mFragment?.let { loadFragment(R.id.container, it) }
        } else {
            mFragment = getFragment(R.id.container) as? MainFragment
        }
    }
}
