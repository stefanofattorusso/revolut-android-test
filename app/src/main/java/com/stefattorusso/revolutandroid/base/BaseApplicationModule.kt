package com.stefattorusso.revolutandroid.base


import android.app.Application
import android.content.Context
import com.stefattorusso.revolutandroid.di.module.ActivityBuilderModule
import com.stefattorusso.revolutandroid.di.module.NetworkModule
import com.stefattorusso.revolutandroid.di.module.RepositoryModule
import com.stefattorusso.revolutandroid.di.module.UseCaseModule
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module(
    includes = arrayOf(
        ActivityBuilderModule::class,
        NetworkModule::class,
        RepositoryModule::class,
        UseCaseModule::class
    )
)
abstract class BaseApplicationModule {

    @Binds
    @Singleton
    internal abstract fun application(application: BaseApplication): Application

    @Binds
    @Singleton
    internal abstract fun context(application: Application): Context
}
