package com.stefattorusso.revolutandroid.data.repository

import com.stefattorusso.revolutandroid.domain.RateDomain
import io.reactivex.Single

interface RatesRepositoryContract {

    fun retrieveLatest(base: String): Single<List<RateDomain>>
}