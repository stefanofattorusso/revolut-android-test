package com.stefattorusso.revolutandroid.di.module

import com.stefattorusso.revolutandroid.di.scope.ActivityScope
import com.stefattorusso.revolutandroid.ui.main.MainActivity
import com.stefattorusso.revolutandroid.ui.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    abstract fun mainActivity(): MainActivity

}