package com.stefattorusso.revolutandroid.di.module

import com.stefattorusso.revolutandroid.domain.usecase.GetLatestUseCase
import com.stefattorusso.revolutandroid.domain.usecase.GetLatestUseCaseContract
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class UseCaseModule {

    @Binds
    @Singleton
    internal abstract fun getLatestUseCase(repository: GetLatestUseCase): GetLatestUseCaseContract
}